$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("busca_produto.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "##language: pt"
    }
  ],
  "line": 3,
  "name": "Acessar o site Shoestock",
  "description": "\t\t\t\t\t\te realizar busca produtos \r\n\t\t\t\t\t\te adicionar os produtos no carrinho,\r\n\t\t\t\t\t\tvalidando os produtos incluídos no carrinho na tela de pagamento.",
  "id": "acessar-o-site-shoestock",
  "keyword": "Funcionalidade"
});
formatter.before({
  "duration": 15635500000,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": "Incluo produtos no carrinho",
  "description": "",
  "id": "acessar-o-site-shoestock;incluo-produtos-no-carrinho",
  "type": "scenario",
  "keyword": "Cenário",
  "tags": [
    {
      "line": 8,
      "name": "@Run"
    }
  ]
});
formatter.step({
  "line": 10,
  "name": "que acesso o site shoestock",
  "keyword": "Dado "
});
formatter.step({
  "line": 11,
  "name": "procuro o produto \"Slip On Couro Shoestock\"",
  "keyword": "Quando "
});
formatter.step({
  "line": 12,
  "name": "adiciono o produto \"Slip On Couro Shoestock\" com tamanho \"43\" no carrinho",
  "keyword": "E "
});
formatter.step({
  "line": 13,
  "name": "valido o produto \"Slip On Couro Shoestock\" no carrinho",
  "keyword": "Quando "
});
formatter.step({
  "line": 14,
  "name": "procuro outro produto \"Slip On Couro Shoestock Camurça\"",
  "keyword": "E "
});
formatter.step({
  "line": 15,
  "name": "adiciono outro produto \"Slip On Couro Shoestock Camurça\" com \"38\" no carrinho",
  "keyword": "E "
});
formatter.step({
  "line": 16,
  "name": "valido produtos \"Slip On Couro Shoestock\" e \"Slip On Couro Shoestock Camurça\" no carrinho na tela de pagamento",
  "keyword": "Então "
});
formatter.match({
  "location": "IncluirProdutosCarrinhoSteps.que_acesso_o_site_shoestock()"
});
formatter.result({
  "duration": 16445744900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Slip On Couro Shoestock",
      "offset": 19
    }
  ],
  "location": "IncluirProdutosCarrinhoSteps.procuro_o_produto(String)"
});
formatter.result({
  "duration": 8798848300,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Slip On Couro Shoestock",
      "offset": 20
    },
    {
      "val": "43",
      "offset": 58
    }
  ],
  "location": "IncluirProdutosCarrinhoSteps.adiciono_o_produto_com_tamanho_no_carrinho(String,String)"
});
formatter.result({
  "duration": 38124266000,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Slip On Couro Shoestock",
      "offset": 18
    }
  ],
  "location": "IncluirProdutosCarrinhoSteps.valido_o_produto_no_carrinho(String)"
});
formatter.result({
  "duration": 8996404200,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Slip On Couro Shoestock Camurça",
      "offset": 23
    }
  ],
  "location": "IncluirProdutosCarrinhoSteps.procuro_outro_produto(String)"
});
formatter.result({
  "duration": 25706907400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Slip On Couro Shoestock Camurça",
      "offset": 24
    },
    {
      "val": "38",
      "offset": 62
    }
  ],
  "location": "IncluirProdutosCarrinhoSteps.adiciono_outro_produto_com_no_carrinho(String,String)"
});
formatter.result({
  "duration": 37625781900,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Slip On Couro Shoestock",
      "offset": 17
    },
    {
      "val": "Slip On Couro Shoestock Camurça",
      "offset": 45
    }
  ],
  "location": "IncluirProdutosCarrinhoSteps.valido_produtos_e_no_carrinho_na_tela_de_pagamento(String,String)"
});
formatter.result({
  "duration": 16054603800,
  "status": "passed"
});
formatter.after({
  "duration": 8036457200,
  "status": "passed"
});
});