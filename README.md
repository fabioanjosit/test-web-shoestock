# Projeto Automação de teste Java/JUnit/Cucumber/Selenium WebDriver

- Selenium Webdriver + Cucumber + Screenshot + Relatório.

# Cenários escritos com BDD em Cucumber em portugues

# O setup de construção e execução:
- [x] Windows 10
- [x] Instalado:
	- Java JDK/JRE 1.8  
	- apache-maven-3.6.2
	- Eclipse Neon.3
	- Gitbash
- [x] chromedriver(A versão para executar o teste, depende 
			da versão do chrome instalado no equipamento)

# O Projeto deve ser importado no Eclipse como "Existing Maven Projects"
<img src="import_project_eclipse.jpg">

# chromedirver.exe
- O navegador utilizado para teste é Chrome, porém para executar o teste, precisa baixar o 
	"chromedriver.exe" de acordo com a verão do chrome instalado no equipamento

- Segue o link para baixar o chromedriver.exe, caso seja necessário.
- [x] https://chromedriver.chromium.org/downloads

- O chromedriver.exe deve ficar na pasta 
		- [x] /src/test/resources/chromedriver.exe

# A execução no Eclipse pode ser feito diretamente no arquivo "RunnerTest.java"
- [x] src/test/java/runners/RunnerTest.java

# Segue a estrutura de pastas do projeto:
<img src="estrutura_pastas.jpg">