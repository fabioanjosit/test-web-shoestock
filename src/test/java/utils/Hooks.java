package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import utils.Setup;
import utils.Setup.IdentifyNav;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import utils.Global;
import utils.Global.Technology;

public class Hooks extends Setup{
	
	public static WebDriver driver;


	public  boolean  FECHAR_NAVEGADOR    = true; 
	
	@Before
	public void start_test(Scenario scenario) throws IOException {
		get_driver();
	}	
	
	@After
	public void FecharNavegador(Scenario scenario) { 
		
		if(Global.document!=null){
			Global.document.close();
		}
		
		if (FECHAR_NAVEGADOR){
			tear_down();
		}

	}
	
	public static void tear_down() { 
		if (driver != null){
			driver.close();
			driver.quit();
			driver = null;
		}
	}
		
	public static WebDriver get_driver() throws IOException { 
		Properties prop = getProp();
		if (driver ==null){
//			driver = setUp(driver, IdentifyNav.CHROME, "https://www.phptravels.net/admin");
//			driver = setUp(driver, navegador, url);
			driver = setUp(driver, IdentifyNav.valueOf(prop.getProperty("prop.website.navegador")), prop.getProperty("prop.website.url"));
		}
		return driver;
	}

	public static Properties getProp() throws IOException {
        Properties props = new Properties();
        FileInputStream file = new FileInputStream("src"+File.separator+
    												"test"+File.separator+
    												"resources"+File.separator+
    												"start.properties");
        props.load(file);
        return props;
 
    }
	
}
