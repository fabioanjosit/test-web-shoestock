package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.poi.util.SystemOutLogger;
import org.easetech.easytest.util.TestInfo;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Key;
import org.sikuli.script.Screen;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import static utils.Helper.*;
import static org.junit.Assert.assertTrue;
import static utils.Hooks.*;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

import utils.Setup;

import java.awt.Desktop.Action;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;


public class Global extends Setup{
	
	public String path = "target//report-html//";
	public static Document document = new Document();

	public Global() {
		// TODO Auto-generated constructor stub
		super();
		try {
			Actions action = new Actions(get_driver());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public enum Identify {
		ID, NAME, XPATH, LINKTEXT, CSS, CLASSNAME;
	}

	public enum Technology {
		SELENIUM_WEB, APPIUM;
	}


	public void fillField(Identify by, String identify_value, String value) {

		WebElement webElement = getElementApplication(by, identify_value);
		webElement.sendKeys(value);
	}

	public void clickButton(Identify by, String identify_value) {

		WebElement webElement = getElementApplication(by, identify_value);
		webElement.click();
	}

	public void selectComboBox(Identify by, String identify_value, String value) {

		WebElement comboBox = getElementApplication(by, identify_value);
		Select combo = new Select(comboBox);
		combo.selectByVisibleText(value);
	}

	public boolean waitElementVisible(Identify by, String identify_value, int seconds) throws IOException {

		WebDriverWait wait = new WebDriverWait(get_driver(), seconds);

		try {

			switch (by) {

			case ID:
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(identify_value)));
				break;

			case NAME:
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(identify_value)));
				break;

			case XPATH:
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(identify_value)));
				break;

			case LINKTEXT:
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(identify_value)));
				break;

			case CSS:
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(identify_value)));
				break;

			case CLASSNAME:
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(identify_value)));
				break;

			default:
				break;
			}

		} catch (Exception e) {
			return false;
		}

		return true;
	}

	public boolean elementExist(Identify by, String identify_value, int seconds) throws IOException {

		List<WebElement> elementos = null;

		for (int i = 0; i < seconds; i++) {

			switch (by) {

			case ID:
				elementos = get_driver().findElements(By.id(identify_value));
				break;

			case NAME:
				elementos = get_driver().findElements(By.name(identify_value));
				break;

			case XPATH:
				elementos = get_driver().findElements(By.xpath(identify_value));
				break;

			case LINKTEXT:
				elementos = get_driver().findElements(By.linkText(identify_value));
				break;

			case CSS:
				elementos = get_driver().findElements(By.cssSelector(identify_value));
				break;

			case CLASSNAME:
				elementos = get_driver().findElements(By.className(identify_value));
				break;

			default:
				break;
			}

			if (elementos.size() > 0)
				return true;

			wait(1);
		}

		return false;
	}

	public void wait(int seconds) {

		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception e) {
		}
	}

	public WebElement getElementApplication(Identify by, String identify_value) {
		WebElement webElement = null;

		try {

			switch (by) {

			case ID:
				webElement = get_driver().findElement(By.id(identify_value));
				break;

			case NAME:
				webElement = get_driver().findElement(By.name(identify_value));
				break;

			case XPATH:
				webElement = get_driver().findElement(By.xpath(identify_value));
				break;

			case LINKTEXT:
				webElement = get_driver().findElement(By.linkText(identify_value));
				break;

			case CSS:
				webElement = get_driver().findElement(By.cssSelector(identify_value));
				break;

			case CLASSNAME:
				webElement = get_driver().findElement(By.className(identify_value));
				break;

			default:
				break;
			}

		} catch (Exception e) {
			Assert.fail("Elemento '" + identify_value + "' NãO encontrado.");
		}

		return webElement;
	}

	public boolean checkElement(String xpath) {
		try {
			return get_driver().findElement(By.xpath(xpath)).isDisplayed();

		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean waitElement(String xpath) throws IOException {
		
		WebDriverWait aguardar = new WebDriverWait(get_driver(), 10);
		return aguardar.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath))).isDisplayed();
	}
	
	
	public void PageUP(String textLink) throws IOException{				
		Actions action = new Actions(get_driver());
		try {
			action.moveToElement(getElementApplication(Identify.NAME,textLink));
			action.sendKeys(Keys.PAGE_UP).build().perform();
		} catch (Exception e) {			
			assertTrue("Não encontrou o Xpath (pageup) no Link ' " +textLink.toString() + " '", false);			
		}

	}
	
	public File evidencia() throws IOException {
		TestInfo testName = new TestInfo(null);
		TakesScreenshot ss = (TakesScreenshot) get_driver();
		File arquivo = ss.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(arquivo, new File(path + File.separator + testName.getTestClass()+ "_" + getDateHour() + ".png"));
		return arquivo;
	}
	
    public void GeraPDF(String TituloReport ) {
    	// criação do objeto documento
        try {

            PdfWriter.getInstance(document, new FileOutputStream("target//report//ReportTest_" +getDateHour() + ".pdf"));
            document.open();

            // adicionando um parágrafo ao documento
            document.add(new Paragraph(TituloReport));

            // adicionando um parágrafo com fonte diferente ao arquivo
            document.add(new Paragraph("#### Report Test ####", FontFactory.getFont(FontFactory.COURIER, 12)));
            Image image = Image.getInstance(evidencia().getAbsolutePath());
            image.scaleAbsolute(530f, 300f);
            document.add(image);
            document.newPage();
            
        } catch(DocumentException de) {
            System.err.println(de.getMessage());
        } catch(IOException ioe) {
            System.err.println(ioe.getMessage());
        } finally {
        }	
    }
    
	public void colarTexto(WebElement elemetoWeb, String textoParaColar){
    	Toolkit toolkit = Toolkit.getDefaultToolkit();
    	Clipboard clipboard = toolkit.getSystemClipboard();
    	try {
    		
    		StringSelection strSelection = new StringSelection(textoParaColar);
    		clipboard.setContents(strSelection, null);
    		elemetoWeb.sendKeys(Keys.CONTROL + "v");
    		
    	} catch(Exception e){
    		System.err.println(e.getMessage());
    	}finally{
    		
    	}
    }
	
	public void loginSikuli(String user, String password){
	
		Screen screen = new Screen();
		try{
			screen.type(user);
			screen.type(Key.TAB);
			screen.type(password);
			screen.type(Key.TAB);
			screen.type(Key.ENTER);
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
		
	}
		
	public void enviaTextoSikuli(String Texto){
		Screen screen = new Screen();
		try{
			screen.type(Texto);
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	public void enviaTabSikuli(){
		Screen screen = new Screen();
		try{
			screen.type(Key.TAB);
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	public void enviaEnterSikuli(){
		Screen screen = new Screen();
		try{
			screen.type(Key.ENTER);
		}catch(Exception e){
			System.err.println(e.getMessage());
		}
	}
	
	
	public static Properties getPropriedades() throws IOException {
        try{
        	Properties props = new Properties();
        	FileInputStream file = new FileInputStream("src"+File.separator+
        			"test"+File.separator+
        			"resources"+File.separator+
        			"start.properties");
        	props.load(file);
        	return props;
        }catch (Exception e) {
        	System.err.println("Ocorreu erro ao carregar arquivo start.properties " +e.getMessage());
        	return null;
		}
    }
 
    public static void  main(String args[]) throws IOException {
        String login; //Variavel que guardará o login do servidor.
        String host; //Variavel que guardará o host do servidor.
        String password; //Variável que guardará o password do usúario.
        System.out.println("************Teste de leitura do arquivo de propriedades************");
         
        Properties prop = getPropriedades();
         
        login = prop.getProperty("prop.server.login");
        host = prop.getProperty("prop.server.host");
        password = prop.getProperty("prop.server.password");
         
        System.out.println("Login = " + login);
        System.out.println("Host = " + host);
        System.out.println("Password = " + password);
    }
	
	
	
}
