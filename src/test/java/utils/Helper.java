package utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static utils.Hooks.get_driver;
import utils.Setup;
import utils.Global.Identify;

public class Helper{


	Global global = new Global();
	
	public Helper() {
		// TODO Auto-generated constructor stub
	}

	public static String readFileTxt(String pathFile) {

		String value = "";
		try {
			BufferedReader file = new BufferedReader(new FileReader(pathFile));

			String line;

			while ((line = file.readLine()) != null) {
				value = value + line;
			}
			file.close();

		} catch (Exception e) {
			Assert.fail("Exceção ao utilizar o arquivo '" + pathFile + "'.");
		}

		return value;
	}

	public static String getDateHour() {
		
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy_HH-mm-ss");
		return dateFormat.format(date);
	}
	
	public static void aguardar_elemento(WebElement elemento, int timeout) { 
		
		WebDriverWait aguardar = null;
		try {
			aguardar = new WebDriverWait(get_driver(), timeout);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		aguardar.until(ExpectedConditions.visibilityOf(elemento));
	}
	
	public static boolean elemento_existe(WebElement elemento, int timeout) { 
		try {			
			aguardar_elemento(elemento, timeout);
			return true;			
		} catch (Exception e) {
			System.out.println("O elemento não encontrado. Segue Mensagem:");
			System.out.println(e.getMessage());
			return false;
		}		
	}
	
}
