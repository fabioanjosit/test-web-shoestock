package pages;

import static utils.Hooks.get_driver;

import java.io.IOException;

import org.eclipse.jdt.internal.compiler.ast.Expression;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utils.*;
import utils.Setup;
import utils.Global.*;

public class HomePage extends Setup{
	
	Global global = new Global();
	
	
	public void iniciaHomePage() throws IOException{
		global.wait(15);
		if (global.elementExist(Identify.ID, "pnosp_div_close", 15)){
			global.GeraPDF("Clico para fechar a propaganda");
			global.clickButton(Identify.ID,"pnosp_div_close");
		}
	}
	
	public void buscaProduto (String textoProduto) throws IOException {
		try{
			if(global.waitElementVisible(Identify.ID,"search-input" ,15)){
				global.clickButton(Identify.ID, "search-input");
				global.fillField(Identify.ID,"search-input",textoProduto);
				global.GeraPDF("Digito o produto para buscar "+textoProduto);
				global.clickButton(Identify.XPATH, "//button[@title='Buscar']");
			}
			global.wait(7);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void clicarItemProduto(String textoItemProduto) throws IOException, InterruptedException{
		global.wait(7);
		if (global.waitElement("//a[@title='"+ textoItemProduto +"']/img")){
			global.wait(7);
			global.GeraPDF("Clico no produto "+textoItemProduto);
			global.clickButton(Identify.XPATH,"//a[@title='"+ textoItemProduto +"']/img");
		}
	}
	
	public void clicarTamanho(String textoTamanho) throws IOException {
		global.wait(5);
		if(global.checkElement("(//a[@data-label='"+textoTamanho+"'])[1]")){
			global.wait(7);
			global.GeraPDF("Clico no tamanho do produto " +textoTamanho);
			global.clickButton(Identify.XPATH, "(//a[@data-label='"+textoTamanho+"'])[1]");
		}
	}
	
	public void clicarBotaoComprar() throws IOException {
		if (global.waitElementVisible(Identify.ID, "buy-button-now", 7)){
			global.wait(7);
			global.GeraPDF("Clico no botão comprar");
			global.clickButton(Identify.ID, "buy-button-now");
		}
	}
	
	public void clicarBotaoMaisProdutos(){
		if(global.checkElement("//a[normalize-space()='Escolher mais produtos']")){
			global.clickButton(Identify.XPATH, "//a[normalize-space()='Escolher mais produtos']");
		}
	}
	
	//========================VALIDACAO======================================//
	public boolean validaItensCarrinho(String textoItemProduto) throws IOException {
		global.wait(7);
		global.GeraPDF("Valido item do produto no carrinho");
		return global.checkElement("//h3[normalize-space()='"+ textoItemProduto+"']");
	}
	
	
}
