package test;

import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.runner.RunWith;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.jfree.data.KeyedValues;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Keyboard;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.*;
import utils.Setup;
import pages.HomePage;

//@RunWith(DataDrivenTestRunner.class)
public class BuscaProdutoTest extends Setup {

	HomePage homePage = new HomePage();
	String 
			itemProduto		= "Slip On Couro Shoestock",
			textoTamanho	= "43",
			itemProduto2	= "Slip On Couro Shoestock Camurça",
			textoTamanho2	= "38";
	
	@Test
	public void buscaProduto() throws InterruptedException, IOException {
		//----primeiro produto
		homePage.iniciaHomePage();
		homePage.buscaProduto(itemProduto);
		homePage.clicarItemProduto(itemProduto);
		homePage.clicarTamanho(textoTamanho);
		homePage.clicarBotaoComprar();
		assertTrue("O produto nao foi encontrado no carrinho", homePage.validaItensCarrinho(itemProduto));
		//----segundo produto
		homePage.clicarBotaoMaisProdutos();
		homePage.iniciaHomePage();
		homePage.buscaProduto(itemProduto2);
		homePage.clicarItemProduto(itemProduto2);
		homePage.clicarTamanho(textoTamanho2);
		homePage.clicarBotaoComprar();
		assertTrue("O produto nao foi encontrado no carrinho", homePage.validaItensCarrinho(itemProduto));
		assertTrue("O produto nao foi encontrado no carrinho", homePage.validaItensCarrinho(itemProduto2));
	}
	
	//---------------------------------SETUP-----------------------------///
	@Before
	public void start_web() throws IOException  { 
		Hooks.getProp();	
		driver = Hooks.get_driver();
	}
	@After
	public void tearDown() {
		Hooks.tear_down();
	}	
	
	
}
