package runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import com.github.mkolisnyk.cucumber.runner.ExtendedTestNGRunner;


@RunWith(ExtendedCucumber.class)
@ExtendedCucumberOptions(
//		jsonReport = "target/report/cucumber.json",
		//retryCount = 3,
		detailedReport = true,
		detailedAggregatedReport = true,
		overviewReport = true,
		coverageReport = true,
//		jsonUsageReport = "target/report/cucumber-usage.json",
		usageReport = true,
		toPDF = true,
		//excludeCoverageTags = {"@flaky" },
		includeCoverageTags = {"@passed" },
		outputFolder = "target"
)
@CucumberOptions(
		features = {"src/test/resources/features/"}, 
		glue = {"steps","utils","pages"},
		tags = {"@Run"},
		monochrome=true,
		plugin = {"pretty", "html:target/report-html"},
		snippets = SnippetType.UNDERSCORE		
		)

public class RunnerTest {

}
