##language: pt

Funcionalidade: Acessar o site Shoestock
								e realizar busca produtos 
								e adicionar os produtos no carrinho,
								validando os produtos incluídos no carrinho na tela de pagamento.
	
	@Run
	Cenário: Incluo produtos no carrinho
			Dado que acesso o site shoestock 
			Quando procuro o produto "Slip On Couro Shoestock"
			E adiciono o produto "Slip On Couro Shoestock" com tamanho "43" no carrinho
			Quando valido o produto "Slip On Couro Shoestock" no carrinho
			E procuro outro produto "Slip On Couro Shoestock Camurça" 
			E adiciono outro produto "Slip On Couro Shoestock Camurça" com "38" no carrinho
			Então valido produtos "Slip On Couro Shoestock" e "Slip On Couro Shoestock Camurça" no carrinho na tela de pagamento
	